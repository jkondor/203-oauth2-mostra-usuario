package br.com.oauth.mostrausuario.controller;

import br.com.oauth.mostrausuario.model.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @GetMapping("/usuario")
    public Usuario mostrarUsuario(@AuthenticationPrincipal Usuario usuario){
        System.out.println(usuario.getName());

        return usuario;

    }
}
