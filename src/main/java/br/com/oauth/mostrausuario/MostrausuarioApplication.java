package br.com.oauth.mostrausuario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MostrausuarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(MostrausuarioApplication.class, args);
	}

}
